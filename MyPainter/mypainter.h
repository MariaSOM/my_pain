#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <QElapsedTimer>

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();

public slots:
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void ActionNegative();
	void ActionBlackWhite();
	void ActionSepiaTone();
	void ActionMedian();
	void ActionSaltPepper();
	void ActionLeft();
	void ActionRight();
	//void Kresli();
	//void Vypln();
	//void NastavFarbu();
	void farbavyplne();
	void farbaciar();
	void kreslikruh();
	void kreslipolynom();
	void scanline();
	void skosenie();
	void skalovanie();
	void otoceniel();
	void otocenier();
	void preklopenie();
	void posuvanie();
	void vymaz();
	void vyfarbi();

private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;

};

#endif // MYPAINTER_H
