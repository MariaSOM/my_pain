#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <vector>
#include <QColorDialog>
#include <QtGlobal>
#include <qvector.h>
#include <stdio.h>

using namespace std;

typedef struct TH
{
	QPoint body;
	int surY;
	double w;

}TH;

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void fastNegative();
	void blackWhite();
	void sepiaTone();
	void medianFilter();
	void saltPepper();
	void RotateLeft();
	void RotateRight();

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

	void body(int bod, double polomer);
	void DDA(int bod, double polomer);
	void BA(int bod, double polomer);
	void DDA_body(QPoint A, QPoint B);
	//QPoint vrat_bod(int index) {return points[index];}
	QVector<QPoint> vrat_bod() { return points; }
	QVector<QPoint> vrat_bod1() { return points1; }
	//	vector<QPoint> vrat_body() { return points; }
	void vyprazdni() { points.clear(); }
	QImage obrazok() { return image; }
	QPoint bod() { return bodik; }
	void scanline();
	QColor farba();
	QColor farba1();
	void otoc(bool pravalava);
	//void otocR();
	void posun();
	void skal(QString koef);
	void skos(QString koef);
	void preklop();
	void vykresliDDA();
	bool farbi(bool ano);

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	int selectKth(int* data, int s, int e, int k);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	QVector<int> x;
	QVector<int> y;
	QVector<QPoint> points;
	QPoint bodik;
	QVector<TH> hrany;
	QVector<TH> a_hrany;
	QColor color;
	QColor color1;
	bool move;
	QPoint posledny;
	QPoint movePoint;
	QVector<QPoint> points1;
	bool scanlinuj = false;

};
#endif // PAINTWIDGET_H
