#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(800, 600);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::ActionNegative()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.fastNegative();
	QString text= "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionBlackWhite()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.blackWhite();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionSepiaTone()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.sepiaTone();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionMedian()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.medianFilter();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionSaltPepper()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.saltPepper();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionLeft()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.RotateLeft();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionRight()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.RotateRight();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::kreslipolynom()
{
	paintWidget.clearImage();
	if (ui.checkBox->isChecked())
	{	
		paintWidget.scanline();
	//	paintWidget.farbi(true);
	}
	//else
		//paintWidget.farbi(false);
		
	paintWidget.vykresliDDA();
}

void MyPainter::kreslikruh()
{
	paintWidget.clearImage();
	paintWidget.body(ui.spinBox_4->value(), ui.doubleSpinBox->value());
	if (ui.comboBox->currentIndex() == 0)
	{
		paintWidget.DDA(ui.spinBox_4->value(), ui.doubleSpinBox->value());
	}
	else if (ui.comboBox->currentIndex() == 1)
	{
		paintWidget.BA(ui.spinBox_4->value(), ui.doubleSpinBox->value());
	}
	
	if (ui.checkBox->isChecked())
		paintWidget.scanline();
	paintWidget.vyprazdni();
	update();
}

void MyPainter::farbavyplne()
{
	QMessageBox mbox;
		paintWidget.farba();
}

void MyPainter::farbaciar()
{
	paintWidget.farba1();
}

void MyPainter::scanline()
{
	paintWidget.clearImage();
	paintWidget.scanline();
	paintWidget.vykresliDDA();
	//paintWidget.vyprazdni();
	//help.clear();
}

void MyPainter::otocenier()
{
	paintWidget.clearImage();
	paintWidget.otoc(true);
	if (ui.checkBox->isChecked())
		paintWidget.scanline();
	paintWidget.vykresliDDA();
}

void MyPainter::otoceniel()
{
	paintWidget.otoc(false);
	paintWidget.clearImage();
	if (ui.checkBox->isChecked())
		paintWidget.scanline();
	paintWidget.vykresliDDA();
}

void MyPainter::skalovanie()
{
	paintWidget.clearImage();
	paintWidget.skal(ui.lineEdit->text());
	if (ui.checkBox->isChecked())
		paintWidget.scanline();
	paintWidget.vykresliDDA();
}

void MyPainter::skosenie()
{
	paintWidget.clearImage();
	paintWidget.skos(ui.lineEdit->text());
	if (ui.checkBox->isChecked())
		paintWidget.scanline();
	paintWidget.vykresliDDA();
}

void  MyPainter::preklopenie()
{
	paintWidget.clearImage();
	paintWidget.preklop();
	if (ui.checkBox->isChecked())
		paintWidget.scanline();
	paintWidget.vykresliDDA();
}

void MyPainter::posuvanie()
{
	paintWidget.clearImage();
	paintWidget.posun();
	if (ui.checkBox->isChecked())
		paintWidget.scanline();
	paintWidget.vykresliDDA();
}

void MyPainter::vymaz()
{
	paintWidget.clearImage();
	paintWidget.vyprazdni();
}

void MyPainter::vyfarbi()
{
	if (ui.checkBox->isChecked())
	{
		paintWidget.farbi(true);
	}
	else
		paintWidget.farbi(false);
}