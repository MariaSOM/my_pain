#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
	points.erase(points.begin(), points.end());
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	points.clear();
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::fastNegative()
{
	int x = image.height();
	int y = image.width();
	unsigned __int64 asd = 0x00ffffff00ffffff;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		QRgb* scanline=(QRgb*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			scanline[j] = 16777215 - scanline[j];
		}
		/*unsigned __int64* scanline=(unsigned __int64*)image.scanLine(i);
		for (int j = 0; j < y/2; j++)
		{
			scanline[j] ^= asd;
		}*/
	}
	update();
}

void PaintWidget::blackWhite()
{

	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			uchar color = (scanline[j * 4 + 0] * 0.11) + (scanline[j * 4 + 1] * 0.59) + (scanline[j * 4 + 2] * 0.3);
			scanline[j * 4 + 0] = color;
			scanline[j * 4 + 1] = color;
			scanline[j * 4 + 2] = color;
		}
	}
	/*for (int i = 0; i < image.width(); i++)
	{
		for (int j = 0; j < image.height(); j++)
		{
			QColor farba = image.pixelColor(i, j);
			farba.setRed(255 - farba.red());
			farba.setBlue(255 - farba.blue());
			farba.setGreen(255 - farba.green());
			image.setPixelColor(i, j, farba);
		}
	}*/
	update();
}

void PaintWidget::sepiaTone()
{
	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			int colorR = scanline[j * 4 + 2] * 0.393 + scanline[j * 4 + 1] * 0.769 + scanline[j * 4 + 0] * 0.189;
			int colorG = scanline[j * 4 + 2] * 0.349 + scanline[j * 4 + 1] * 0.686 + scanline[j * 4 + 0] * 0.168;
			int colorB = scanline[j * 4 + 2] * 0.272 + scanline[j * 4 + 1] * 0.534 + scanline[j * 4 + 0] * 0.131;
			scanline[j * 4 + 2] = colorR > 255 ? 255 : colorR;
			scanline[j * 4 + 1] = colorG > 255 ? 255 : colorG;
			scanline[j * 4 + 0] = colorB > 255 ? 255 : colorB;
		}
	}
	/*
	outputRed = (inputRed * .393) + (inputGreen *.769) + (inputBlue * .189)
outputGreen = (inputRed * .349) + (inputGreen *.686) + (inputBlue * .168)
outputBlue = (inputRed * .272) + (inputGreen *.534) + (inputBlue * .131)*/
	/*for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			QRgb farba = image.pixel(j, i);
			image.setPixel(j, i, 16777215 - farba);
		}
	}*/
	update();
}

void PaintWidget::medianFilter()
{
	
	int x = image.height();
	int y = image.width();
	uchar *data = image.bits();
	int riadok = image.bytesPerLine();
#pragma omp parallel
	{
		int reds[9];
		int greens[9];
		int blues[9];
#pragma omp for
		for (int i = 1; i < x - 1; i++)
		{
			for (int j = 1; j < y - 1; j++)
			{
				int counter = 0;
				for (int k = -1; k < 2; k++)
					for (int l = -1; l < 2; l++)
					{
						blues[counter] = data[(i + k)*riadok + (j + l) * 4 + 0];
						greens[counter] = data[(i + k)*riadok + (j + l) * 4 + 1];
						reds[counter] = data[(i + k)*riadok + (j + l) * 4 + 2];
						counter++;
					}
				data[i*riadok + j * 4 + 0] = blues[selectKth(blues, 0, 9, 4)];
				data[i*riadok + j * 4 + 1] = greens[selectKth(greens, 0, 9, 4)];
				data[i*riadok + j * 4 + 2] = reds[selectKth(reds, 0, 9, 4)];
			}
		}
	}
}

void PaintWidget::saltPepper()
{
	int count = image.width()*image.height()*0.1;
	QRgb *data = (QRgb *)image.bits();
	int riadok = image.bytesPerLine()/4;
#pragma omp parallel
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> xsur(0, image.width() - 1);
		std::uniform_int_distribution<> ysur(0, image.height() - 1);
#pragma omp for
		for (int i = 0; i < count; i++)
		{
			data[ysur(gen)*riadok +xsur(gen)] = i % 2 == 0 ? 0x00000000 : 0x00ffffff;
		}
	}
}

void PaintWidget::RotateLeft()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine()/4;
	int riadok2 = druhy.bytesPerLine()/4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[i + (y-j-1)*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::RotateRight()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine() / 4;
	int riadok2 = druhy.bytesPerLine() / 4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[x - i - 1 + j*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::clearImage()
{
	//points.clear();
	image.fill(qRgb(255, 255, 255));
	modified = true;
	
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
	else if (event->button() == Qt::RightButton)
	{
		movePoint = event->pos();
		move = true;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
	else if ((event->buttons() & Qt::RightButton) && move)
	{
	//	drawLineTo(event->pos());
		//posun();
		//clearImage();
		//vykresliDDA();
	}		
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
	painting = false;
	}
	else if (event->button() == Qt::RightButton && move)
	{
		posledny = event->pos();
		posun();
		clearImage();
		if (scanlinuj == true)
			scanline();
		vykresliDDA();
		move = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	//painter.drawLine(lastPoint, endPoint);
	modified = true;

	//int rad = (myPenWidth / 2) + 2;
	//update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	//lastPoint = endPoint;
	myPenWidth = 5;
	painter.drawPoint(lastPoint);
	myPenWidth = 1;
	points.push_back(lastPoint);
	printf("%d %d\n", lastPoint.x(), lastPoint.y());
	update();
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

void PaintWidget::body(int bod, double polomer)
{
	int xs = image.width();
	int ys = image.height();
	QPainter painter(&image);
	painter.setPen(QPen(QColor("blue"), myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(QColor("blue")));

	//painter.drawEllipse(round((xs / 2)-3), round((ys / 2) - 3), 6, 6);

	painter.setPen(QPen(QColor("red"), myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(QColor("red")));

	for (int i = 0; i < bod; i++)
	{
		//double hodnota = polomer - i*((polomer) / (body));
		double vysledok = i * 2 * M_PI / bod;
		double x1, y1;
		x1 = (xs / 2 + (polomer)*cos(vysledok));
		y1 = (ys / 2 + (polomer)*sin(vysledok));

		/*	QMessageBox mbox;
			mbox.setText("x1: " + QString::number(x1) + " y1: " + QString::number(y1));
			mbox.exec();*/

		x.push_back(round(x1));
		y.push_back(round(y1));
//		painter.drawEllipse(round(x1)-3, round(y1)-3, 6, 6);
	}
	update();
}

void PaintWidget::DDA(int bod, double polomer)
{
	QPainter painter(&image);
	double m = 0, helpxx = 0, helpyy = 0;
	painter.setPen(QPen(color1, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(color1));
	for (int i = 0; i < x.size(); i++)
	{
		double helpx = 0, helpy = 0, dx, dy;

		if (i != x.size() - 1)
		{
			helpx = x[i];
			helpy = y[i];
			helpxx = x[i + 1];
			helpyy = y[i + 1];
			dx = abs(x[i + 1] - x[i]);
			dy = abs(y[i + 1] - y[i]);
		}
		else
		{
			helpx = x[i];
			helpy = y[i];
			helpxx = x[0];
			helpyy = y[0];
			dx = abs(x[0] - x[i]);
			dy = abs(y[0] - y[i]);
		}

		if (helpx > helpxx)
		{
			swap(helpx, helpxx);
			swap(helpy, helpyy);
		}
		else if(helpx == helpxx && helpy > helpyy)
		{
			swap(helpy, helpyy);
		}

		if (helpx == helpxx)
		{
			while (helpy < helpyy)
			{
				helpy = (helpy + 1);
				painter.drawEllipse(round(helpx), round(helpy), 2, 2);
			}
			continue;
		}

		m = dy / dx;

		if (fabs(m) <= 1 /*&& m != 0)*/)
		{				
				while (helpx < helpxx)
				{
					if (helpx == helpxx && helpy > helpyy)
						helpy = (helpy - 1);
					else
						helpx = (helpx + 1);
					if (helpy < helpyy)
						helpy = (helpy + fabs(m));
					else if (helpy > helpyy)
						helpy = (helpy - fabs(m));

					painter.drawEllipse(round(helpx), round(helpy), 2, 2);
				}
		}
		else if (fabs(m) > 1)
		{
			while (helpx < helpxx)
			{
				helpx = (helpx + 1 / fabs(m));
				if (helpy < helpyy)
					helpy = (helpy + 1);
				else
					helpy = (helpy - 1);
				painter.drawEllipse(round(helpx), round(helpy), 2, 2);
			}
		}
	}

	x.erase(x.begin(), x.begin() + x.size());
	y.erase(y.begin(), y.begin() + y.size());
	update();
}

void PaintWidget::BA(int bod, double polomer)
{
	QPainter painter(&image);
	painter.setPen(QPen(color1, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(color1));

	for (int i = 0; i < x.size(); i++)
	{
		int helpx = 0, helpy = 0;
		int helpxx = 0, helpyy = 0;

		if (i != x.size() - 1)
		{
			helpx = x[i];
			helpy = y[i];
			helpxx = x[i + 1];
			helpyy = y[i + 1];
		}
		else
		{
			helpx = x[i];
			helpy = y[i];
			helpxx = x[0];
			helpyy = y[0];
		}

		int dx = fabs(helpxx - helpx);
		int	dy = fabs(helpyy - helpy);
		int ky1 = 2 * dy;
		int ky2 = 2 * dy - 2*dx;
		int kx1 = 2 * dx;
		int kx2 = 2 * dx - 2*dy;

		int py = 2 * dy - dx;
		int px = 2 * dx - dy;

		painter.drawEllipse(helpx, helpy, 2, 2);

		if (helpx > helpxx)
		{
			swap(helpx, helpxx);
			swap(helpy, helpyy);
		}
		else if (helpx == helpxx && helpy > helpyy)
		{
			swap(helpy, helpyy);
		}

		if (helpx == helpxx)
		{
			while (helpy < helpyy)
			{
				helpy = (helpy + 1);
				painter.drawEllipse(round(helpx), round(helpy), 2, 2);
			}
			continue;
		}

		if (fabs((double)dy / (double)dx) <= 1 /*&& fabs((double)dy / (double)dx) != 0*/)	
		{
			while (helpx < helpxx)
			{
				helpx = helpx + 1;
				if (py > 0)
				{
					if(helpy < helpyy)
						helpy = helpy + 1;
					else
						helpy = helpy - 1;
					py = py + ky2;
				}
				else
					py = py + ky1;
				painter.drawEllipse(helpx, helpy, 2, 2);
			}
		}
		else if (fabs((double)dy / (double)dx) == 0 || dx == 0)
		{
			while (helpx == helpxx && helpy < helpyy)
			{
				helpy = (helpy + 1);
				painter.drawEllipse(helpy, helpx, 2, 2);
			}
			while (helpx == helpxx && helpy > helpyy)
			{
				helpy = (helpy - 1);
				painter.drawEllipse(helpx, helpy, 2, 2);
			}
		}
		else if (fabs((double)dy / (double)dx) > 1)
		{
			while (helpx < helpxx)
			{
				if (helpy < helpyy)
					helpy = helpy + 1;
				else
					helpy = helpy - 1;

				if (px > 0)
				{
					helpx = helpx + 1;
					px = px + kx2;
				}
				else
					px = px + kx1;
				painter.drawEllipse(helpx, helpy, 2, 2);
			}
		}
		while (helpx == helpxx && helpy < helpyy)
		{
			helpy = (helpy + 1);
			painter.drawEllipse(round(helpx), round(helpy), 2, 2);
		}
		while (helpx == helpxx && helpy > helpyy)
		{
			helpy = (helpy - 1);
			painter.drawEllipse(round(helpx), round(helpy), 2, 2);
		}
	}
	x.erase(x.begin(), x.begin() + x.size());
	y.erase(y.begin(), y.begin() + y.size());
	update();
}

void PaintWidget::DDA_body(QPoint A, QPoint B)
{
	QPainter painter(&image);
	double m = 0, helpxx = 0, helpyy = 0;
	painter.setPen(QPen(color1, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(color1));
	double helpx = 0, helpy = 0, dx, dy;

	helpx = A.x();
	helpy = A.y();
	helpxx = B.x();
	helpyy = B.y();
	dx = abs(B.x() - A.x());
	dy = abs(B.y() - A.y());

	if (helpx > helpxx)
	{
		swap(helpx, helpxx);
		swap(helpy, helpyy);
	}
	else if (helpx == helpxx && helpy > helpyy)
	{
		swap(helpy, helpyy);
	}

	if (helpx == helpxx)
	{
		while (helpy < helpyy)
		{
			helpy = (helpy + 1);
			painter.drawEllipse(round(helpx), round(helpy), 2, 2);
		}
		return;
	}

	m = dy / dx;

	if (fabs(m) <= 1 /*&& m != 0)*/)
	{
		while (helpx < helpxx)
		{
			if (helpx == helpxx && helpy > helpyy)
				helpy = (helpy - 1);
			else
				helpx = (helpx + 1);
			if (helpy < helpyy)
				helpy = (helpy + fabs(m));
			else if (helpy > helpyy)
				helpy = (helpy - fabs(m));

			painter.drawEllipse(round(helpx), round(helpy), 2, 2);
		}
	}
	else if (fabs(m) > 1)
	{
		while (helpx < helpxx)
		{
			helpx = (helpx + 1 / fabs(m));
			if (helpy < helpyy)
				helpy = (helpy + 1);
			else
				helpy = (helpy - 1);
			painter.drawEllipse(round(helpx), round(helpy), 2, 2);
		}
	}
	update();
}

bool func(const TH& a, const TH& b)
{
	if (a.body.y() != b.body.y())
		return a.body.y() < b.body.y();
	else if (a.body.y() == b.body.y())
	{		
		if (a.body.x() == b.body.x())
			return a.w < b.w;
		return a.body.x() < b.body.x();
	}
}

bool func1(const TH& a, const TH& b)
{
	if (a.body.x() != b.body.x())
		return a.body.x() < b.body.x();
	else if (a.body.x() == b.body.x())
	{
			return a.w < b.w;
	}
}

void PaintWidget::scanline()
{
	int helpx, helpy, helpxx, helpyy;
	double dx, dy;
	int ymax = 0, ymin = 0;
	TH hlp;
	QPainter painter(&image);
	QVector<int> pries;

	painter.setPen(QPen(color, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	//painter.setBrush(QBrush(color));

	printf("Points size: %d", points.size());
	for (int i = 0; i < points.size(); i++)
	{
		printf("y: %d\n", points[i].y());
		if (i != points.size() - 1)
		{
			helpx = points[i].x();
			helpy = points[i].y();
			helpxx = points[i + 1].x();
			helpyy = points[i + 1].y();
		}
		else
		{
			helpx = points[i].x();
			helpy = points[i].y();
			helpxx = points[0].x();
			helpyy = points[0].y();
		}

		if ((helpy != helpyy))
		{
			if (helpy > helpyy)
			{
				swap(helpx, helpxx);
				swap(helpy, helpyy);
			}

			helpyy = helpyy - 1;			///??

			dx = (helpxx - helpx);
			dy = (helpyy - helpy);

			hlp.body.setX(helpx);
			hlp.body.setY(helpy);
			hlp.surY = helpyy;
			hlp.w = 1.0 / ((double)dy / (double)dx);

			hrany.push_back(hlp);
			ymax = hlp.surY > ymax ? hlp.surY : ymax;
		}
	}

/*	QMessageBox mbox;
	printf("hrany size: %d\n", hrany.size());
	*/
	/*for(int i = 0; i < hrany.size(); i ++)
		printf("hrany pred vytriedenim: %d %d %f\n", hrany[i].body.x(), hrany[i].body.y(), hrany[i].w);
		*/
	sort(hrany.begin(), hrany.end(), func);

	/*for (int i = 0; i < hrany.size(); i++)
		printf("hrany po vytriedeni: %d %d %f\n", hrany[i].body.x(), hrany[i].body.y(), hrany[i].w);
		*/
		QPoint help, help1;
		help.setX(hrany[0].body.x());
		help.setY(hrany[0].body.y());
		help1.setX(hrany[0].body.x());
		help1.setY(hrany[0].body.y());
		
		ymin = help.y();

	//	printf("max: %d %d", hrany[hrany.size() - 1].body.x(), hrany[hrany.size() - 1].surY);

		while (ymin <= ymax)
	//	for (int k = help.y(); k <= hrany[hrany.size() - 1].surY; k++)
		{
			for (int i = 0; i <= hrany.size()-1; i++)
			{
				if (hrany[i].body.y() == ymin)
				{
					hlp.body.setX(hrany[i].body.x());
					hlp.body.setY(hrany[i].body.y());
					hlp.surY = hrany[i].surY;
					hlp.w = hrany[i].w;
					//a_hrany.push_back(hlp);
					a_hrany.append(hlp);
					
			/*		mbox.setText("Pridavam hranu: " + QString::number(a_hrany[i].body.x()) + " " + QString::number(a_hrany[i].body.y()) + " hrany: " + QString::number(a_hrany.size()));
					mbox.exec();
*/
	//					printf("aktivne hrany: %d %d %f\n", a_hrany[i].body.x(), a_hrany[i].body.y(), a_hrany[i].w);

				}

			}

			//	sort(a_hrany.begin(), a_hrany.end(), func);
				//sort(a_hrany.begin(), a_hrany.end(), func1);
	
				
				for (int i = 0; i <= a_hrany.size()-1; i+=2)
				{
					/*
						help.setX(a_hrany[i].w * ymin - a_hrany[i].w * a_hrany[i].body.y() + a_hrany[i].body.x());
						help1.setX(a_hrany[i + 1].w * ymin - a_hrany[i + 1].w * a_hrany[i + 1].body.y() + a_hrany[i + 1].body.x());
						*/
						pries << a_hrany[i].w * ymin - a_hrany[i].w * a_hrany[i].body.y() + a_hrany[i].body.x();
						pries << a_hrany[i + 1].w * ymin - a_hrany[i + 1].w * a_hrany[i + 1].body.y() + a_hrany[i + 1].body.x();

						/*
						if(help.x() > help1.x())
							painter.drawLine(help1.x(), ymin, help.x(), ymin);
						else if (help.x() < help1.x())
							painter.drawLine(help.x(), ymin, help1.x(), ymin);
							*/
						//DDA_body(QPoint(help1.x(), ymin), QPoint(help.x(), ymin));
				}

				qSort(pries);

				if (pries.size() % 2) {
					printf("Neparny pocet priesecnikov!\n");
				}

				for (int i = 0; i < pries.size(); i = i + 2) {
					if (pries[i] < pries[i+1])
						painter.drawLine(pries[i+1], ymin, pries[i], ymin);
					else 
						painter.drawLine(pries[i], ymin, pries[i+1], ymin);
				}

				pries.clear();

				int i = 0;
				while (i <= a_hrany.size() - 1)
				{
					if (ymin == a_hrany[i].surY)
					{
						a_hrany.erase(a_hrany.begin() + i);
					}
					else
						i++;
				}
			//help.setY(help.y() + 1);
				ymin++;
		}
		a_hrany.clear();
		hrany.clear();
	}

	QColor PaintWidget::farba()
	{
		color = QColorDialog::getColor(Qt::white, this);
		return color;
	}

	QColor PaintWidget::farba1()
	{
		color1 = QColorDialog::getColor(Qt::white, this);
		return color1;
	}

void PaintWidget::otoc(bool pravalava)
{
	QPoint hlp;
	points1.clear();
	hlp.setX(points[0].x());
	hlp.setY(points[0].y());
	printf("%d %d\n", hlp.x(), hlp.y());
	points1.push_back(hlp);

	if (pravalava == true)
	{
		for (int i = 1; i < points.size(); i++)
		{
			hlp.setX((points[i].x() - points[0].x())*cos(M_PI /12) - (points[i].y() - points[0].y())*sin(M_PI / 12) + points[0].x());
			hlp.setY((points[i].x() - points[0].x())*sin(M_PI / 12) + (points[i].y() - points[0].y())*cos(M_PI / 12) + points[0].y());
			points1.push_back(hlp);
			printf("%d %d\n", hlp.x(), hlp.y());
		}
	}
	else if (pravalava == false)
	{
		for (int i = 1; i < points.size(); i++)
		{
			hlp.setX((points[i].x() - points[0].x())*cos(M_PI / 12) + (points[i].y() - points[0].y())*sin(M_PI / 12) + points[0].x());
			hlp.setY(-(points[i].x() - points[0].x())*sin(M_PI / 12) + (points[i].y() - points[0].y())*cos(M_PI / 12) + points[0].y());
			points1.push_back(hlp);
			printf("%d %d\n", hlp.x(), hlp.y());
		}
	}

	points.clear();
	points = points1;
	update();
}

void PaintWidget::skal(QString koef)
{
	int sx, sy;
	QPoint hlp;
	float d;
	if (koef == "")
		d = 1.;
	else
		d = koef.toDouble();
	sx = points[0].x();
	sy = points[0].y();
	points1.clear();
	hlp.setX(points[0].x());
	hlp.setY(points[0].y());
	vector<int> dlzka;	

	printf("%d %d\n", hlp.x(), hlp.y());
	points1.push_back(hlp);

	for (int i = 1; i < points.size(); i++)
	{
		hlp.setX(((points[i].x() - sx) * (d)) + sx);
		hlp.setY(((points[i].y() - sy) * (d)) + sy);
		points1.push_back(hlp);
		printf("%d %d\n", hlp.x(), hlp.y());
	}

	points.clear();
	points = points1;
	update();
}

void PaintWidget::skos(QString koef)
{
	int sx, sy;
	QPoint hlp;
	float d;
	if (koef == "")
		d = 1.;
	else
		d = koef.toDouble();
	sx = points[0].x();
	sy = points[0].y();
	points1.clear();
	hlp.setX(points[0].x());
	hlp.setY(points[0].y());
	printf("%d %d\n", hlp.x(), hlp.y());
	points1.push_back(hlp);

	for (int i = 1; i < points.size(); i++)
	{
		hlp.setX((((points[i].x() - sx) - (d)*(points[i].y() - sy))) + sx);
		hlp.setY(points[i].y());
		points1.push_back(hlp);
		printf("%d %d\n", hlp.x(), hlp.y());
	}

	points.clear();
	points = points1;
	update();
}

void PaintWidget::preklop()
{
	int sx, sy;
	QPoint hlp;
	QPainter painter(&image);
	sx = points[0].x();
	sy = points[0].y();
	points1.clear();
	painter.drawLine(0, sy, image.width(), sy);
	hlp.setX(sx);
	hlp.setY(sy);
	printf("%d %d\n", hlp.x(), hlp.y());
	points1.push_back(hlp);

	painter.drawLine(0, sy, image.width(), sy);

	for (int i = 1; i < points.size(); i++)
	{
		hlp.setX(points[i].x());
		hlp.setY(2*sy-points[i].y());
		points1.push_back(hlp);
		printf("%d %d\n", hlp.x(), hlp.y());
	}

	points.clear();
	points = points1;
	update();
}


void PaintWidget::posun()
{
	QPoint hlp;
	int sx = points[0].x();
	int sy = points[0].y();
	points1.clear();
	printf("%d %d\n", movePoint.x(), movePoint.y());
		for (int i = 0; i < points.size(); i++)
		{
				hlp.setX(points[i].x() + (posledny.x() - sx));
				hlp.setY(points[i].y() + (posledny.y() - sy));
				points1.push_back(hlp);
		}
	points.clear();
	points = points1;
	update();

}

void PaintWidget::vykresliDDA()
{
	for (int i = 0; i < points.size(); i++)
	{
		if (i != points.size() - 1)
			DDA_body(points[i], points[i + 1]);
		else
			DDA_body(points[i], points[0]);
	}
}

bool PaintWidget::farbi(bool ano)
{
	if (ano == true)
	{
		scanlinuj = true;
		return scanlinuj;
	}
		
	else
	{
		scanlinuj = false;
		return scanlinuj;
	}
}